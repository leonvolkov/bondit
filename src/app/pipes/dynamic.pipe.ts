import {Injector, Pipe, PipeTransform, InjectionToken, Type} from '@angular/core';
import { DatePipe, PercentPipe, DecimalPipe, CurrencyPipe } from '@angular/common';

@Pipe({
  name: 'dynamicPipe'
})
export class DynamicPipe implements PipeTransform {

    public constructor(private injector: Injector) {
    }

    transform(value: any, metadata: any): any {
        if (!metadata || !metadata.format) {
            return value;
        } else {
            let pipeToken: Pipe = null;
            const pipeArgs: string[] = [];

            switch (metadata.data_type) {
              case 'number':
              // ? period still unused
              case 'time_period':  {
                pipeToken = DecimalPipe;
                if (metadata.format.digits_info) {
                  pipeArgs.push(metadata.format.digits_info);
                }
                break;
              }
              case 'mul100_percent': {
                pipeToken = PercentPipe;
                if (metadata.format.digits_info) {
                  pipeArgs.push(metadata.format.digits_info);
                }
                break;
              }
              case 'date': {
                pipeToken = DatePipe;
                if (metadata.format.date_format) {
                  pipeArgs.push(metadata.format.date_format);
                }
                break;
              }
              default: {
                return value;
                break;
              }
            }
            const pipe = this.injector.get(pipeToken);

            return pipe.transform(value, ...pipeArgs);
        }
    }
}
