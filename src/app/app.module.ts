import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { DatePipe, PercentPipe, DecimalPipe, CurrencyPipe, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TreeTableModule } from 'primeng/treetable';
import { ButtonModule } from 'primeng/button';
import { InputTextModule } from 'primeng/inputtext';
import { DropdownModule } from 'primeng/dropdown';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TreeDataGridComponent } from './tree-data-grid/tree-data-grid.component';
import { GridMetadataService } from './services/grid-metadata.service';
import { GridDataService } from './services/grid-data.service';
import { DynamicPipe } from './pipes/dynamic.pipe';
import { DataGridControllerComponent } from './data-grid-controller/data-grid-controller.component';

@NgModule({
  declarations: [
    AppComponent,
    TreeDataGridComponent,
    DynamicPipe,
    DataGridControllerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    TreeTableModule,
    ButtonModule,
    InputTextModule,
    DropdownModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: PathLocationStrategy },
    GridMetadataService,
    GridDataService,
    DatePipe,
    PercentPipe,
    DecimalPipe,
    CurrencyPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
