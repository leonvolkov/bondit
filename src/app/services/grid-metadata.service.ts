import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GridMetadataService {

  constructor(private http: HttpClient) { }

  public getGridMetadata(url: string) {
    return this.http.get(url);
  }
}
