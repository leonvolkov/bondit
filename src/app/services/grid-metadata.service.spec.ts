import { TestBed } from '@angular/core/testing';

import { GridMetadataService } from './grid-metadata.service';

describe('GridMetadataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GridMetadataService = TestBed.get(GridMetadataService);
    expect(service).toBeTruthy();
  });
});
