import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GridFilterService {

  constructor(private http: HttpClient) { }

  public getGridFilterData(url: string) {
    return this.http.get(url);
  }
}
