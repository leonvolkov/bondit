import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';

import {TreeNode, SortEvent} from 'primeng/api';
import { TreeTable } from 'primeng/treetable';

import { ObjectUtils } from 'primeng/components/utils/objectutils';

import { GridMetadataService } from '../services/grid-metadata.service';
import { GridDataService } from '../services/grid-data.service';
import { GridFilterSelect } from '../data-grid-controller/grid-filter-select';

@Component({
  selector: 'app-tree-data-grid',
  templateUrl: './tree-data-grid.component.html',
  styleUrls: ['./tree-data-grid.component.scss']
})
export class TreeDataGridComponent implements OnInit {
  @Input() metadata_url: string;
  @Input() metadata_key: string;
  @Input() data_url: string;
  @Input() data_key: string;
  @Input() title: string;
  @ViewChild(TreeTable) public treeTableGrid: TreeTable;

  private subscriptions = new Subscription();
  private gridData: TreeNode[] = [];
  private gridMetadata: Object = {};
  private runtime: Object = {};
  private colNames: string[] = [];
  private colData: Object = {};
  private filter_data_url: string = '/assets/data/filterData.json';
  private filters_data_keys: string[];


  public static stringify(str: string) {
    return JSON.stringify(str);
  }

  constructor(
    private gridMetadataService: GridMetadataService,
    private gridDataService: GridDataService
  ) { }

  ngOnInit() {
    this.subscriptions.add(this.gridMetadataService.getGridMetadata(this.metadata_url)
      .subscribe(this.setGridMetedata.bind(this)));
    this.subscriptions.add(this.gridDataService.getGridData(this.data_url)
      .subscribe(this.setGridData.bind(this)));

    this.treeTableGrid.isFilterMatched = function (node, _a) {
        const filterField = _a.filterField,
          filterValue = _a.filterValue,
          filterConstraint = _a.filterConstraint,
          isStrictMode = _a.isStrictMode;
        let matched = false;
        const dataFieldValue = ObjectUtils.resolveFieldData(node, filterField).value;
        if (filterConstraint(dataFieldValue, filterValue)) {
            matched = true;
        }
        if (!matched || (isStrictMode && !this.isNodeLeaf(node))) {
            matched = this.findFilteredNodes(node, {
              filterField: filterField, filterValue: filterValue,
              filterConstraint: filterConstraint, isStrictMode: isStrictMode }) || matched;
        }
        return matched;
    };
  }

  private setGridMetedata(metadata: object) {
    this.gridMetadata = metadata;

    const componentKey = metadata['components'][this.metadata_key]['sections'][0];
    const sectionAttriutes = metadata['sections'][componentKey];

    for (const attrSet in sectionAttriutes) {
      if (sectionAttriutes.hasOwnProperty(attrSet)) {
        this.colNames = [...this.colNames, ...sectionAttriutes[attrSet]];
        this.colData = {...this.colData, ...metadata[attrSet]};
      }
    }

    const filterSectionKey = metadata['components']['table_filters']['sections'][0];
    this.filters_data_keys = metadata['sections'][filterSectionKey]['form_elements'];
  }

  private setGridData(data: object) {
    this.gridData = data[this.data_key].items;
    this.runtime = data['runtime'];
  }

  private applyGridFilters(allFilters: any) {
    const selectedFilters = allFilters.selectedFilters;
    const globalFilterKeyword = allFilters.globalFilterKeyword;

    this.resetFilters();
    for (const filter in selectedFilters) {
      if (selectedFilters.hasOwnProperty(filter)) {
        const treeKey = filter.replace('portfolio_', 'portfolio_attribute_');
        if (selectedFilters[filter].key) {
          this.treeTableGrid.filter(selectedFilters[filter].label, treeKey, 'equals');
        }
      }
    }
    if (globalFilterKeyword) {
      this.treeTableGrid.filterGlobal(globalFilterKeyword, 'contains');
    }
  }

  private gridSort(event: SortEvent) {
        // event.data = Data to sort
        // event.mode = 'single' or 'multiple' sort mode
        // event.field = Sort field in single sort
        // event.order = Sort order in single sort
        // event.multiSortMeta = SortMeta array in multiple sort

        event.data.sort((data1, data2) => {
            const value1 = data1[event.field].value;
            const value2 = data2[event.field].value;
            let result = null;

            if (value1 == null && value2 != null) {
                result = -1;
            } else if (value1 != null && value2 == null) {
                result = 1;
            } else if (value1 == null && value2 == null) {
                  result = 0;
            } else if (typeof value1 === 'string' && typeof value2 === 'string') {
                result = value1.localeCompare(value2);
            } else {
                result = (value1 < value2) ? -1 : (value1 > value2) ? 1 : 0;
            }
            return (event.order * result);
        });
    }

    private resetFilters() {
        this.treeTableGrid.filters = null;
        this.treeTableGrid.reset();
    }

}
