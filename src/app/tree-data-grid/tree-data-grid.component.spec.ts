import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TreeDataGridComponent } from './tree-data-grid.component';

describe('TreeDataGridComponent', () => {
  let component: TreeDataGridComponent;
  let fixture: ComponentFixture<TreeDataGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeDataGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeDataGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
