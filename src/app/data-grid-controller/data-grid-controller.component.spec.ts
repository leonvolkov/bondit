import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DataGridControllerComponent } from './data-grid-controller.component';

describe('DataGridControllerComponent', () => {
  let component: DataGridControllerComponent;
  let fixture: ComponentFixture<DataGridControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DataGridControllerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DataGridControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
