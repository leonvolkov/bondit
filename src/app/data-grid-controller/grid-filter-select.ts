export interface GridFilterSelect {
  key: string;
  label: string;
}
