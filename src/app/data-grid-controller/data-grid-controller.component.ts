import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import {SelectItem} from 'primeng/api';
import { GridFilterSelect } from './grid-filter-select';

import { GridFilterService } from '../services/grid-filter.service';

@Component({
  selector: 'app-data-grid-controller',
  templateUrl: './data-grid-controller.component.html',
  styleUrls: ['./data-grid-controller.component.scss']
})
export class DataGridControllerComponent implements OnInit {
  @Input() filters_data_keys: string[];
  @Input() filter_data_url: string;
  @Output() applyGridFilters = new EventEmitter<any>();

  private subscriptions = new Subscription();
  private gridFilterData: Object = {};
  private selectedFilters: Object = {};
  private globalFilterKeyword: string = '';
  private filtersDataLoaded: boolean = false;

  constructor(
    private gridFilterService: GridFilterService
  ) { }

  ngOnInit() {
    this.subscriptions.add(this.gridFilterService.getGridFilterData(this.filter_data_url)
      .subscribe(this.setGridFilterData.bind(this)));
  }

  private setGridFilterData(data: object) {
    this.gridFilterData = data;
    for (const key of this.filters_data_keys) {
      this.selectedFilters[key] = <GridFilterSelect>this.gridFilterData[key].data[0];
    }
    this.filtersDataLoaded = true;
  }

  private gridFilterOnChange(event) {
    const allFilters = {
      selectedFilters: this.selectedFilters,
      globalFilterKeyword: this.globalFilterKeyword
    };
    this.applyGridFilters.emit(allFilters);
  }

  private gridGlobalFilterButtonOnClick(event) {
    event.preventDefault();
  }

}
